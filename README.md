diskio-pi-battery-meter
=======================

Install on Raspberry PI
------------------------

First, install the development packages:

```
sudo apt install raspberrypi-kernel-headers device-tree-compiler
```

Add a module to load in /etc/modules
```
i2c-dev
i2c-bcm2708
```
Add a module to load in /boot/config.txt
```
dtparam=i2c_arm=on
dtoverlay=i2c-rtc,ds1307
```

Reboot

Build the driver :

```
git clone https://gitlab.com/diskiopi/diskio-pi-battery-meter.git
cd diskio-pi-battery-meter
make
```

Install the driver:

```
sudo insmod max1726x_battery.ko
```

Build and install the overlay:

```
dtc -O dtb -o max1726x_battery.dtbo -b 0 -@ max1726x_battery.dts
sudo dtoverlay max1726x_battery.dtbo
```

![an example of the applet](indicateur_batt.jpg)

NOTE: At the moment, the driver is not calibrated, so the information displayed may not match.

If you want the battery meter to start when the Raspberry PI start, please, follow this procedure
------------------------

Modify /boot/config.txt and add

```
dtparam=i2c_arm=on
dtoverlay=i2c-rtc,ds1307
```

Modify /etc/modules and add

```
i2c-dev
i2c-bcm2708
```

Create a .sh file. Example: batterie.sh. We will create it in the home folder in our example

```
nano ~/batterie.sh
```

Insert following lines:

```
#! /bin/bash

#créer un fichier log
exec >/home/pi/batterie.log 2>&1

sleep 5
/sbin/insmod /home/pi/diskio-pi-battery-meter/max1726x_battery.ko
sleep 2
sudo -i -u pi dtc -O dtb -o /home/pi/diskio-pi-battery-meter/max1726x_battery.dtbo -b 0 -@ /home/pi/diskio-pi-battery-meter/max1726x_battery.dts
sleep 2
dtoverlay /home/pi/diskio-pi-battery-meter/max1726x_battery.dtbo
```

Make the .sh runnable

```
chmod +x ~/batterie.sh
```

In order to run the script when the device starts, we are using crontab. You need to start it as root user so the script will run with root credentials

```
sudo crontab -e
```

Add

```
@reboot /home/pi/batterie.sh
```

Save.

Reboot.

If nothing is displayed, add the battery applet, 2 designs are available.
